#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h> // Don't forget to compile with "-lm"

typedef uint64_t u64;

bool is_prime(u64 num, u64 cont) {
    if (num <= 2) return true;
    if (num % cont != 0) return is_prime(num, ++cont);
    // In case the number > 49, we only need to check down to sqrt(number).
    // If it didn't find any divisor down to sqrt(number), then the number is prime
    if (cont > (u64)sqrt(num) && num > 49) return true;
    if (num == cont) return true;
    else return false;
}

void main(void) {
    u64 weight;
    scanf("%ld", &weight);

    u64 primes[100] = {0};
    for (u64 i=2, j=0; i<weight; i++) {
        if (is_prime(i, 2)) {
            primes[j] = i;
            j++;
        }
    }

    bool flag = true;
    for(u64 i = 0; primes[i] !=0 ; i++) {
        u64 w = weight / primes[i];
        if(w%2 == 0) {
            printf("YES\n");
            flag = false;
            break;
        }
    }

    if (flag) {
        printf("NO\n");
    }
}
