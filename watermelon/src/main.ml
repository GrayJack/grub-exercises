#use "topfind"
#thread
#require "containers"

open Containers

let is_prime num =
    let num = abs num in
    let rec is_not_divisor d =
      d * d > num || (num mod d <> 0 && is_not_divisor (d+1)) in
    num <> 1 && is_not_divisor 2

let primes_less_than num =
    let primes = [] in
    for i = 2 to num do
        if is_prime num then primes@[i]
        else primes@[];
    done;
    primes

let () =
    let weight = read_int () in
    let primes = primes_less_than weight in
    let len = List.length primes in
    let flag = ref true in
    for i=0 to len-1 do
        let prime = List.nth primes i in
        let w = weight / prime in
        if w mod 2 = 0 then flag := false
    done;

    if !flag then Printf.printf "NO\n"
    else Printf.printf "YES\n"
