#!/bin/env janet

(defn prime?
    [num cont]
    (cond
        (<= num 2) true
        (not= (% num cont) 0) (prime? num (+ cont 1))
        (and (> cont (math/sqrt num)) (> num 49)) true
        (== num cont) true
        false))

(defn primes-less-than
    [num]
    (var primes @[])
    (for i 2 (+ num 1)
        (when (prime? i 2)
            (array/push primes i)))
    primes)

(def weight (buffer/popn (:read stdin :line) 1))
(def weight (scan-number weight))
(def primes (primes-less-than weight))
(var flag true)
(loop [prime :in primes]
    (def w (math/floor (/ weight prime)))
    (when (= (% w 2) 0)
        (set flag false)))

(if flag
    (print "NO")
    (print "YES"))
