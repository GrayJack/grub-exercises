#!/bin/env julia

function is_prime(x::UInt128, cont::UInt128)::Bool
    if x <= 2
        return true
    end
    if x%cont != 0
        return is_prime(x, cont+1)
    end
    if cont > sqrt(x) && x > 49
        return true
    end
    if x == cont
        return true
    else
        return false
    end
end

function primes_less_than(num::UInt128)
    primes = []
    for i = 2:num
        if is_prime(i, UInt128(2))
            push!(primes, i)
        end
    end
    return primes
end

weight = parse(UInt128, readline())
primes = primes_less_than(UInt128(100))

flag = true
for prime in primes
    w = weight // prime
    if w % 2 == 0
        global flag = false
        println("YES")
        break
    end
end

if flag
    println("NO")
end
