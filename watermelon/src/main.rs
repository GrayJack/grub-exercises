use std::{
    error::Error,
    io::stdin,
};

fn is_prime(num: u128, cont: u128) -> bool {
    if num <= 2 { return true };
    if num%cont != 0 { return is_prime(num, cont+1) };
    // In case the number > 49, we only need to check down to sqrt(number).
    // If it didn't find any divisor down to sqrt(number), then the number is prime.
    if cont > (f64::sqrt(num as f64) as u128) && num > 49 { return true };
    if num == cont { return true }
    false
}

fn primes_less_than(num: u128) -> Vec<u128> {
    let mut primes = Vec::new();
    for i in 2..=num {
        if is_prime(i, 2) {
            primes.push(i);
        }
    }
    primes
}

fn main() -> Result<(), Box<Error>> {
    let mut weight = String::new();
    stdin().read_line(&mut weight)?;
    let weight: u128 = weight.trim().parse()?;

    // We only need to get primes less or equal to weight, since every prime bigger than weight
    // weigth/prime == 0
    let primes = primes_less_than(weight);

    let mut flag = true;
    for prime in primes {
        let w = weight/prime;
        if w % 2 == 0 {
            println!("YES");
            flag = false;
            break;
        }
    }

    if flag { println!("NO") }

    Ok(())
}
