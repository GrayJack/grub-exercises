#include <stdio.h>
#include <stdint.h>

void main(void) {
    uint64_t num;
    scanf("%ld", &num);
    printf("%lu\n", num*num);
}
