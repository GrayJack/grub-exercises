fn main() -> Result<(), Box<std::error::Error>> {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input)?;

    let num: i128 = input.trim().parse()?;

    println!("{}", num*num);

    Ok(())
}
