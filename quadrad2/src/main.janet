#!/bin/env janet

(def num (buffer/popn (:read stdin :line) 1))
(def num (scan-number num))
(print (* num num))
