#!/bin/env julia

num = parse(Int128, readline())
println("$(num*num)")
