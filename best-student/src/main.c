#include <stdio.h>

typedef double f64;

void main(void) {
    f64 pedro, paulo;
    scanf("%lf %lf", &pedro, &paulo);
    (pedro <= paulo) ? printf("Pedro\n") : printf("Paulo\n");
}
