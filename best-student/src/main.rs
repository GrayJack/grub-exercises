use std::{
    error::Error,
    io::stdin,
};

fn read() -> Result<(f64, f64), Box<Error>> {
    let mut src = String::new();
    stdin().read_line(&mut src)?;
    let src: Vec<_> = src.split_whitespace().collect();
    Ok((src[0].parse()?, src[1].parse()?))
}

fn main() -> Result<(), Box<Error>> {
    let (pedro, paulo) = read()?;
    if pedro <= paulo {
        println!("Pedro");
    } else {
        println!("Paulo");
    }
    Ok(())
}
