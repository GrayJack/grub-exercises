#!/bin/env janet

(def input
    (map scan-number (string/split " " (buffer/popn (:read stdin :line) 1))))

(let
    [pedro (input 0)
    paulo (input 1)]
    (if (<= pedro paulo)
        (print "Pedro")
        (print "Paulo")))
