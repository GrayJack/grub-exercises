#!/bin/env julia

input = split(readline(), " ")
(pedro, paulo) = (parse(Float64, input[1]), parse(Float64, input[2]))
pedro <= paulo ? println("Pedro") : println("Paulo")
