#use "topfind"
#require "containers"

open Containers
open List

let () =
    let input = read_line () in
    let input = String.split " " input
        |> map Float.of_string in
    let (pedro, paulo) = ((List.nth input 0), (List.nth input 1)) in
    if (int_of_float pedro) <= (int_of_float paulo) then
        Printf.printf "Pedro\n"
    else Printf.printf "Paulo\n"
