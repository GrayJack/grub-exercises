use std::{
    error::Error,
    io::stdin,
};

fn area(tuple: (i128, i128)) -> i128 {
    tuple.0 * tuple.1
}

fn read() -> Result<(i128, i128), Box<Error>> {
    let mut src = String::new();
    stdin().read_line(&mut src)?;
    let src: Vec<_> = src.split_whitespace().collect();
    Ok((src[0].parse()?, src[1].parse()?))
}

fn main() -> Result<(), Box<Error>> {
    let first = read()?;
    let second = read()?;

    let first = area(first);
    let second = area(second);

    if first > second {
        println!("Primeiro");
    } else if first < second {
        println!("Segundo");
    } else {
        println!("Empate");
    }

    Ok(())
}
