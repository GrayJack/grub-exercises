#!/bin/env janet

(defn area [w h]
    (* w h))

(def first_in
    (map scan-number (string/split " " (buffer/popn (:read stdin :line) 1))))
(def first_area (area (first_in 0) (first_in 1)))

(def second_in
    (map scan-number (string/split " " (buffer/popn (:read stdin :line) 1))))
(def second_area (area (second_in 0) (second_in 1)))

(cond
    (> first_area second_area) (print "Primeiro")
    (< first_area second_area) (print "Segundo")
    (print "Empate"))
