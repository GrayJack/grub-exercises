#!/bin/env julia

function area(tuple)
    tuple[1] * tuple[2]
end

first_in = split(readline(), " ")
first = (parse(Int128, first_in[1]), parse(Int128, first_in[2]))
first_area = area(first)

second_in = split(readline(), " ")
second = (parse(Int128, second_in[1]), parse(Int128, second_in[2]))
second_area = area(second)

if first_area > second_area
    println("Primeiro")
elseif first_area < second_area
    println("Segundo")
else
    println("Empate")
end
