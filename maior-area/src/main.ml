#use "topfind"
#require "containers"

open Containers
open List

let () =
    let first_in = read_line () in
    let first_in = String.split " " first_in
        |> map Int.of_string
        |> map (Option.get_or ~default:0) in
    let second_in = read_line () in
    let second_in = String.split " " second_in
        |> map Int.of_string
        |> map (Option.get_or ~default:0) in

    let first_area = (List.nth first_in 0) * (List.nth first_in 1) in
    let second_area = (List.nth second_in 0) * (List.nth second_in 1) in
    if first_area > second_area then Printf.printf "Primeiro\n"
    else if first_area < second_area then Printf.printf "Segundo\n"
    else Printf.printf "Empate\n"
