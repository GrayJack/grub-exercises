#include <stdint.h>
#include <stdio.h>

typedef int64_t i64;

i64 area(i64 w, i64 h) {
    return w * h;
}

void main(void) {
    i64 w1, h1, w2, h2;
    scanf("%ld %ld", &w1, &h1);
    scanf("%ld %ld", &w2, &h2);

    i64 area1 = area(w1, h1);
    i64 area2 = area(w2, h2);

    if (area1 > area2) {
        printf("Primeiro\n");
    } else if (area1 < area2) {
        printf("Segundo\n");
    } else {
        printf("Empate\n");
    }
}
